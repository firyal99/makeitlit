using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public void play()
    {
        SceneManager.LoadScene("Gameplay");
        ScoreScript.scorenum = 0;
    }

    public void setting()
    {
        SceneManager.LoadScene("Settings");
    }

    public void info()
    {
        SceneManager.LoadScene("Info");
    }

    public void fires()
    {
        SceneManager.LoadScene("Achievement");
    }
}