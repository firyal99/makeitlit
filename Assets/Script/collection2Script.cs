using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collection2Script : MonoBehaviour
{
     public GameObject fire1;
    public GameObject fire2;
    public GameObject fire3;
    public GameObject fire4;
    public GameObject fire5;
    public GameObject fire6;
    // Start is called before the first frame update
    void Start()
    {
        
        if(PlayerPrefs.GetInt("Wind Destroyed",0) < 10)
        {
        fire1.SetActive(false);
        fire2.SetActive(false);
        fire3.SetActive(false);
        fire4.SetActive(false);
        fire5.SetActive(false);
        fire6.SetActive(false);
        }

        if(PlayerPrefs.GetInt("Wind Destroyed",0) >= 10)
        {
        fire1.SetActive(true);
        fire2.SetActive(false);
        fire3.SetActive(false);
        fire4.SetActive(false);
        fire5.SetActive(false);
        fire6.SetActive(false);
        }
        if(PlayerPrefs.GetInt("Wind Destroyed",0) >= 20)
        {
        fire1.SetActive(true);
        fire2.SetActive(true);
        fire3.SetActive(false);
        fire4.SetActive(false);
        fire5.SetActive(false);
        fire6.SetActive(false);
        }

        if(PlayerPrefs.GetInt("Wind Destroyed",0) >= 30)
        {
        fire1.SetActive(true);
        fire2.SetActive(true);
        fire3.SetActive(true);
        fire4.SetActive(false);
        fire5.SetActive(false);
        fire6.SetActive(false);
        }
        if(PlayerPrefs.GetInt("Wind Destroyed",0) >= 40)
        {
        fire1.SetActive(true);
        fire2.SetActive(true);
        fire3.SetActive(true);
        fire4.SetActive(true);
        fire5.SetActive(false);
        fire6.SetActive(false);
        }
        if(PlayerPrefs.GetInt("Wind Destroyed",0) >= 50)
        {
        fire1.SetActive(true);
        fire2.SetActive(true);
        fire3.SetActive(true);
        fire4.SetActive(true);
        fire5.SetActive(true);
        fire6.SetActive(false);
        }
        if(PlayerPrefs.GetInt("Wind Destroyed",0) >= 60)
        {
        fire1.SetActive(true);
        fire2.SetActive(true);
        fire3.SetActive(true);
        fire4.SetActive(true);
        fire5.SetActive(true);
        fire6.SetActive(true);
        }

        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }
}
