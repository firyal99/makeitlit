using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class inPauseScript : MonoBehaviour
{
    public GameObject PauseUI;
    //public GameObject GameoverUI;

    public bool paused = false;
    //private bool gameover= false;
    

    private void Start()
    {
        PauseUI.SetActive(false);
        //GameoverUI.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
        }

        if (paused)
        {
            PauseUI.SetActive(true);
            Time.timeScale = 0;
        }
        if (!paused)
        {
            PauseUI.SetActive(false);
            Time.timeScale = 1;
        }

        if(healthScript.CurrentHealth < 0)
        {
            FireScript.gameover = true;
            Time.timeScale = 0;
            
        }
    }
    public void ButtonPause()
        {
            paused = true;
        }

    public void Resume()
    {
        paused = false;
    }

    public void gotoMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void retry()
    {
        SceneManager.LoadScene("Gameplay");
    }
}
