using System.Linq; //access last list
using System.Collections.Generic;
using UnityEngine;

public class WordsScript : MonoBehaviour
{
    private List<string> originalWords = new List<string>()
    {
        "game", "studio", "jakarta",
         "arcade", "rpg", "redrain", "indie", "mobile", "consideration", "time","example",
        "late", "remote", "growing", "high", "confuse", "complicated", "linking", "amaze", "sometimes", "excited", "tone", "contract", "but",
        "advocate", "atleast", "prove", "first", "endorse", "start", "drama", "makeitlit", "fire",
        "ocean", "skies", "grounds", "festival", "artist", "leaders", "leader",
        "integrity", "rich", "bike", "reliance", "fine","hostile","spontaneous","lack","water","wind","gravel","hypothesis","precedent",
        "review","fire","desert","pavement","trap","neutral","consultation","unpleasent","plane",
        "slap","slump","cup","forward","lamb","abuse","cave","healthy","corn","flawed","physical","mastermind",
        "separate","speculate","ratio","extinct","give","hunting","formation","representative","teacher",
        "god","noise","confrontation","company","corner","gradual", "manage", "unit", "radio", "television"
        ,"power", "snap" ,"resort", "edge" ,"flour", "knowledge", "slime"
    };

    private List<string> workingWords = new List<string>();

    private void Awake()
    {
        workingWords.AddRange(originalWords);
        Shuffle(workingWords);
        GoBottom(workingWords);
    }

    private void Shuffle(List<string> list)
    {
        for(int i = 0; i < list.Count; i++)
        {
            int random = Random.Range(i, list.Count);
            string temporary = list[i];

            list[i] = list[random];
            list[random] = temporary;
        }
    }

    private void GoBottom(List<string> list)
    {
        for(int i = 0; i < list.Count; i++)
        list[i] = list[i].ToLower();
    }

    public string GetWord()
    {
        string newWord = string.Empty;

        if(workingWords.Count != 0)
        {
            workingWords.AddRange(originalWords);
            Shuffle(workingWords);
            newWord = workingWords.Last();
            workingWords.Remove(newWord);
        }

        return newWord;
    }


   // public string GetWordAgain()
    //{
        //if(workingWords.Count == 0)
       // {
     //   }
    //    return GetWord();
    //}
}
