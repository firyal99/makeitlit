using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindTyperScript : MonoBehaviour
{
    public NumScript numKeeper = null;
    public Text numberOutput = null;

    public static string RemainingWord2 = string.Empty;
    private string CurrentWord2 = string.Empty;

    // Start is called before the first frame update
    private void Start()
    {
        SetCurrentWord2();
    }

    private void SetCurrentWord2()
    {
        CurrentWord2 = numKeeper.GetWord();
        SetRemainingWord2(CurrentWord2);
    }

    private void SetRemainingWord2(string newString2)
    {
        RemainingWord2 = newString2;
        numberOutput.text = RemainingWord2;
    }
    // Update is called once per frame
    private void Update()
    {
        CheckInput2();
    }

    public void CheckInput2()
    {
        if(Input.anyKeyDown)
        {
            
            string keysPressed2 = Input.inputString;

            if (keysPressed2.Length == 1)
                EnterLetter2(keysPressed2);
        }
    }

    public void EnterLetter2(string TypedLetter2)
    {
        if(Time.timeScale != 0)
        {
        if(IsCorrectLetter2(TypedLetter2))
        {

            //SoundScript.playwindsound();
            //windscript.winddestroy = 0;
            
            //Destroy(this.gameObject);
            ScoreScript.winddestroyed += 1;
            //if(healthScript.CurrentHealth < 200)
            //{
            //    healthScript.CurrentHealth += 20;
            //}
            
            //healthScript.decrease +=2;
            //FireScript.transform.localScale += Vector3(1,1,1);
            RemoveLetter2();
            

            if(IsWordComplete2())
            
            SetCurrentWord2();
        }
        }
    }

    public bool IsCorrectLetter2(string Letter2)
    {
        
        return RemainingWord2.IndexOf(Letter2) == 0;
    }

    public void RemoveLetter2()
    {
        
        string newString2 = RemainingWord2.Remove(0, 1);
        SetRemainingWord2(newString2);
    }

    public bool IsWordComplete2()
    {
        Destroy(GameObject.FindWithTag("wind"));
        SoundScript.playwindsound();
        return RemainingWord2.Length == 0;
        
        
    }


}
