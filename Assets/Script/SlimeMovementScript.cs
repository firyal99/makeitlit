using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeMovementScript : MonoBehaviour
{
    Rigidbody2D rb;
    GameObject target;
    float moveSpeed;
    Vector3 directionToTarget;
   // public GameObject explosion;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("fire");
        rb = GetComponent<Rigidbody2D>();
        moveSpeed = Random.Range(0.7f,1.2f);
    }

    // Update is called once per frame
    void Update()
    {
        MoveSlime();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "fire")
        {
            SoundScript.playslimesound();
            Destroy(this.gameObject);
            healthScript.CurrentHealth -= 100;
        }
    }

    void MoveSlime()
    {
        if(target != null)
        {
            directionToTarget = (target.transform.position - transform.position)
            .normalized;
            rb.velocity = new Vector2(directionToTarget.x * moveSpeed, directionToTarget.y
            * moveSpeed);
        }
        else
        rb.velocity = Vector3.zero;
    }
}
