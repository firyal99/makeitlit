using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.Rendering.Universal; //jgn lupa ini

public class teslightintensity : MonoBehaviour
{
    public Light2D Light;
 
    
    void start()
    {
        Light.intensity = 1f;
    }
    void Update()
    {
        if(healthScript.CurrentHealth > 0)
        {
            Light.intensity -= 0.10f * Time.deltaTime;
        }

        //if(Input.anyKeyDown)
        //{
        //    Light.intensity += 0.1f;
        //}
        if(healthScript.CurrentHealth > 200)
        {
            Light.intensity = 1f;
        }
        
        if(healthScript.CurrentHealth <= 0)
        {
            Light.intensity = 0f;
        }
    }
}
