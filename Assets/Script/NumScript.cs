using System.Linq; //access last list
using System.Collections.Generic;
using UnityEngine;

public class NumScript : MonoBehaviour
{
    private List<string> originalWords = new List<string>()
    {
        "1","2","3","4","5","6","7","8","9","0"

    };

    private List<string> workingWords = new List<string>();

    private void Awake()
    {
        workingWords.AddRange(originalWords);
        Shuffle(workingWords);
        GoBottom(workingWords);
    }

    private void Shuffle(List<string> list)
    {
        for(int i = 0; i < list.Count; i++)
        {
            int random = Random.Range(i, list.Count);
            string temporary = list[i];

            list[i] = list[random];
            list[random] = temporary;
        }
    }

    private void GoBottom(List<string> list)
    {
        for(int i = 0; i < list.Count; i++)
        list[i] = list[i].ToLower();
    }

    public string GetWord()
    {
        string newWord = string.Empty;

        if(workingWords.Count != 0)
        {
            workingWords.AddRange(originalWords);
            Shuffle(workingWords);
            newWord = workingWords.Last();
            workingWords.Remove(newWord);
        }

        return newWord;
    }


   // public string GetWordAgain()
    //{
        //if(workingWords.Count == 0)
       // {
     //   }
    //    return GetWord();
    //}
}
