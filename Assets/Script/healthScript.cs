using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healthScript : MonoBehaviour
{
    
    public static float CurrentHealth;
    public int maxHealth = 200;
    public static int decrease = 1;

    public HealthBar health;
    // Start is called before the first frame update
    void Start()
    {
        //health = GetComponent<Image>();
        CurrentHealth = maxHealth;
        health.SetMaxHealth(maxHealth);
        
    }

    // Update is called once per frame
    void Update()
    {
     if(Time.timeScale != 0)
     {   
        if(CurrentHealth > 0)
        {
            TakeDamage(0.3f);
        }

        else if(CurrentHealth > 200)
        {
            //health.SetMaxHealth(maxHealth);
            //CurrentHealth = maxHealth;
            health.SetHealth(maxHealth);
        }
        
        //health.fillamount = CurrentHealth/MaxHealth;
     }
    }

    void TakeDamage(float damage)
    {
        CurrentHealth -= damage;
        decrease -= 3;
        health.SetHealth(CurrentHealth);

    }
    
}
