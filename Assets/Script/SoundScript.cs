using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundScript : MonoBehaviour
{
    public static AudioClip typesound;
    public static AudioClip windsound;
    public static AudioClip slimesounds;
    static AudioSource audioSrc;
    // Start is called before the first frame update
    void Start()
    {
        typesound = Resources.Load<AudioClip>("typesound");
        windsound = Resources.Load<AudioClip>("Wind2");
        slimesounds = Resources.Load<AudioClip>("slimesound");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public static void playsound()
    {
        
        audioSrc.PlayOneShot(typesound);
    }

    public static void playwindsound()
    {
        
        audioSrc.PlayOneShot(windsound);
    }

    public static void playslimesound()
    {
        audioSrc.PlayOneShot(slimesounds);
    }
}
