using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collectionScript : MonoBehaviour
{
    public GameObject fire1;
    public GameObject fire2;
    public GameObject fire3;
    public GameObject fire4;
    public GameObject fire5;
    public GameObject fire6;
    // Start is called before the first frame update
    void Start()
    {
        
        if(PlayerPrefs.GetInt("Highscore",0) < 100)
        {
        fire1.SetActive(false);
        fire2.SetActive(false);
        fire3.SetActive(false);
        fire4.SetActive(false);
        fire5.SetActive(false);
        fire6.SetActive(false);
        }

        if(PlayerPrefs.GetInt("Highscore",0) >= 100)
        {
        fire1.SetActive(true);
        fire2.SetActive(false);
        fire3.SetActive(false);
        fire4.SetActive(false);
        fire5.SetActive(false);
        fire6.SetActive(false);
        }
        if(PlayerPrefs.GetInt("Highscore",0) >= 500)
        {
        fire1.SetActive(true);
        fire2.SetActive(true);
        fire3.SetActive(false);
        fire4.SetActive(false);
        fire5.SetActive(false);
        fire6.SetActive(false);
        }

        if(PlayerPrefs.GetInt("Highscore",0) >= 1000)
        {
        fire1.SetActive(true);
        fire2.SetActive(true);
        fire3.SetActive(true);
        fire4.SetActive(false);
        fire5.SetActive(false);
        fire6.SetActive(false);
        }
        if(PlayerPrefs.GetInt("Highscore",0) >= 1500)
        {
        fire1.SetActive(true);
        fire2.SetActive(true);
        fire3.SetActive(true);
        fire4.SetActive(true);
        fire5.SetActive(false);
        fire6.SetActive(false);
        }
        if(PlayerPrefs.GetInt("Highscore",0) >= 2000)
        {
        fire1.SetActive(true);
        fire2.SetActive(true);
        fire3.SetActive(true);
        fire4.SetActive(true);
        fire5.SetActive(true);
        fire6.SetActive(false);
        }
        if(PlayerPrefs.GetInt("Highscore",0) >= 2500)
        {
        fire1.SetActive(true);
        fire2.SetActive(true);
        fire3.SetActive(true);
        fire4.SetActive(true);
        fire5.SetActive(true);
        fire6.SetActive(true);
        }

        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }
}
