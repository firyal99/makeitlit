using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class windscript : MonoBehaviour
{
    public GameObject[] wind;
    public Transform[] spawnPoint;
    int randomSpawnPoint,randomWind;
    //public static int winddestroy=0;
    public static bool spawnAllowed;

    // Start is called before the first frame update
    void Start()
    {
        spawnAllowed = true;
        InvokeRepeating ("SpawnWind", 4f, 8f);
    }

    // Update is called once per frame
    void SpawnWind()
    {
       if(spawnAllowed)
       {
           randomSpawnPoint = Random.Range(0, spawnPoint.Length);
           randomWind = Random.Range(0, wind.Length);
           Instantiate(wind[randomWind], spawnPoint[randomSpawnPoint].position,Quaternion.identity);
           //winddestroy = 1;
       }
    }
}
