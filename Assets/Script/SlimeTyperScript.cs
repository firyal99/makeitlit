using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlimeTyperScript : MonoBehaviour
{
    public SymScript SymKeeper = null;
    public Text SymOutput = null;

    public static string RemainingWord3 = string.Empty;
    private string CurrentWord3 = string.Empty;

    // Start is called before the first frame update
    private void Start()
    {
        SetCurrentWord3();
    }

    private void SetCurrentWord3()
    {
        CurrentWord3 = SymKeeper.GetWord();
        SetRemainingWord3(CurrentWord3);
    }

    private void SetRemainingWord3(string newString3)
    {
        RemainingWord3 = newString3;
        SymOutput.text = RemainingWord3;
    }
    // Update is called once per frame
    private void Update()
    {
        CheckInput3();
    }

    public void CheckInput3()
    {
        if(Input.anyKeyDown)
        {
            
            string keysPressed3= Input.inputString;

            if (keysPressed3.Length == 1)
                EnterLetter3(keysPressed3);
        }
    }

    public void EnterLetter3(string TypedLetter3)
    {
        if(Time.timeScale != 0)
        {
        if(IsCorrectLetter3(TypedLetter3))
        {

            //SoundScript.playwindsound();
            //windscript.winddestroy = 0;
            
            //Destroy(this.gameObject);
            ScoreScript.slimedestroyed += 1;
            //if(healthScript.CurrentHealth < 200)
            //{
            //    healthScript.CurrentHealth += 20;
            //}
            
            //healthScript.decrease +=2;
            //FireScript.transform.localScale += Vector3(1,1,1);
            RemoveLetter3();
            

            if(IsWordComplete3())
            
            SetCurrentWord3();
        }
        }
    }

    public bool IsCorrectLetter3(string Letter3)
    {
        
        return RemainingWord3.IndexOf(Letter3) == 0;
    }

    public void RemoveLetter3()
    {
        
        string newString3 = RemainingWord3.Remove(0, 1);
        SetRemainingWord3(newString3);
    }

    public bool IsWordComplete3()
    {
        Destroy(GameObject.FindWithTag("slime"));
        SoundScript.playslimesound();
        return RemainingWord3.Length == 0;
        
        
    }
}
