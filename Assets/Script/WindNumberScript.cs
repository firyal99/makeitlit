using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindNumberScript : MonoBehaviour
{
    public Text numberW = null;
    public GameObject OnWind;
    private List<string> originalNum = new List<string>()
    {
        "dsaw", "dwa", "ddwa"
    };

    private List<string> workingNum = new List<string>();

    private void Awake()
    {
        workingNum.AddRange(originalNum);
        Shuffle(workingNum);
        GoBottom(workingNum);
    }

    private void Shuffle(List<string> list)
    {
        for(int i = 0; i < list.Count; i++)
        {
            int random = Random.Range(i, list.Count);
            string temporary = list[i];

            list[i] = list[random];
            list[random] = temporary;
        }
    }

    private void GoBottom(List<string> list)
    {
        for(int i = 0; i < list.Count; i++)
        list[i] = list[i].ToLower();
    }

    public string GetWord()
    {
        string newNum = string.Empty;

        if(workingNum.Count != 0)
        {
            workingNum.AddRange(originalNum);
            Shuffle(workingNum);
            newNum = workingNum.Last();
            workingNum.Remove(newNum);
        }

        return newNum;
    }
}
