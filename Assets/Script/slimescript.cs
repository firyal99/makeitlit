using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class slimescript : MonoBehaviour
{
    public GameObject[] slime;
    public Transform[] spawnPoint;
    int randomSpawnPoint,randomSlime;
    public static bool spawnAllowed;

    // Start is called before the first frame update
    void Start()
    {
        spawnAllowed = true;
        InvokeRepeating ("SpawnSlime", 5f, 10f);
    }

    // Update is called once per frame
    void SpawnSlime()
    {
       if(spawnAllowed)
       {
           randomSpawnPoint = Random.Range(0, spawnPoint.Length);
           randomSlime = Random.Range(0, slime.Length);
           Instantiate(slime[randomSlime], spawnPoint[randomSpawnPoint].position,Quaternion.identity);
       }
    }
}
