using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TyperScript : MonoBehaviour
{
    public WordsScript wordKeeper = null;
    //public NumScript numKeeper = null;
    public Text wordOutput = null;
    //public Text numberOutput = null;
    public static string keyword = string.Empty;

    public static string RemainingWord = string.Empty;
    private string CurrentWord = string.Empty;

    // Start is called before the first frame update
    private void Start()
    {
        SetCurrentWord();
    }

    private void SetCurrentWord()
    {
        CurrentWord = wordKeeper.GetWord();
        SetRemainingWord(CurrentWord);
    }

    private void SetRemainingWord(string newString)
    {
        RemainingWord = newString;
        wordOutput.text = RemainingWord;
    }
    // Update is called once per frame
    private void Update()
    {
        CheckInput();
    }

    public void CheckInput()
    {
        if(Input.anyKeyDown)
        {
            
            string keysPressed = Input.inputString;

            if (keysPressed.Length == 1)
                EnterLetter(keysPressed);
        }
    }

    public void EnterLetter(string TypedLetter)
    {
        if(Time.timeScale != 0)
        {
        if(IsCorrectLetter(TypedLetter))
        {
            SoundScript.playsound();
            ScoreScript.scorenum += 1;
            if(healthScript.CurrentHealth < 200)
            {
                healthScript.CurrentHealth += 20;
            }
            
            healthScript.decrease +=2;
            FireSkinScript.skinNumber = Random.Range(0,4);
            //FireScript.transform.localScale += Vector3(1,1,1);
            RemoveLetter();
            

            if(IsWordComplete())
            SetCurrentWord();
        }
        }
    }

    public bool IsCorrectLetter(string Letter)
    {
        
        return RemainingWord.IndexOf(Letter) == 0;
    }

    public void RemoveLetter()
    {
        
        string newString = RemainingWord.Remove(0, 1);
        SetRemainingWord(newString);
    }

    public bool IsWordComplete()
    {
        
        return RemainingWord.Length == 0;
    }


}
