using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class windMovementScript : MonoBehaviour
{
    Rigidbody2D rb;
    GameObject target;
    float moveSpeed;
    Vector3 directionToTarget;
   // public GameObject explosion;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("fire");
        rb = GetComponent<Rigidbody2D>();
        moveSpeed = Random.Range(1f,2f);
    }

    // Update is called once per frame
    void Update()
    {
        MoveWind();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "fire")
        {
            SoundScript.playwindsound();
            Destroy(this.gameObject);
            healthScript.CurrentHealth -= 30;
        }

        //if(windscript.winddestroy == 0)
        //{
        //    SoundScript.playwindsound();
            
        //}
        //switch (col.gameObject.tag)
        //{
            
            //case "fire":
            //windscript.spawnAllowed = false;
            //Instantiate(explosion, col.gameObject.transform.position, Quaternion.identity);
            //Destroy(col.gameObject);
            //Destroy(gameObject);
            
            //target = null;
            //break;

           // case "fire":
           // Instantiate(explosion, transform.position, Quaternion.identity);
            //Destroy(col.gameObject);
            //Destroy(gameObject);
            //healthScript.CurrentHealth -= 10;
            //break;

            
        //}
    }

    void MoveWind()
    {
        if(target != null)
        {
            directionToTarget = (target.transform.position - transform.position)
            .normalized;
            rb.velocity = new Vector2(directionToTarget.x * moveSpeed, directionToTarget.y
            * moveSpeed);
        }
        else
        rb.velocity = Vector3.zero;
    }
}
