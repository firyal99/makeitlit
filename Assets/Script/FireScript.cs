using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireScript : MonoBehaviour
{
  public GameObject GameoverUI;
  public static int Transform;
  public static bool gameover = false;

  void Start()
  {
    GameoverUI.SetActive(false);
    gameover = false;

  }
    //public float ShrinkDuration = 10f;
 
  // The target scale
 // public Vector3 TargetScale = Vector3.one * .5f;
 
  // The starting scale
  //Vector3 startScale;
 
  // T is our interpolant for our linear interpolation.
  //float t = 0;
 
  //void OnEnable() {
      //if(Input.anyKeyDown)
      //{
      //transform.localScale -= new Vector3(1, 1, 1);
      //}

    // initialize stuff in OnEnable
   // startScale = transform.localScale;
   // t = 0;
  //}
 
  public void Update() {
    // Divide deltaTime by the duration to stretch out the time it takes for t to go from 0 to 1.
    //t += Time.deltaTime / ShrinkDuration;
 
    // Lerp wants the third parameter to go from 0 to 1 over time. 't' will do that for us.
    //Vector3 newScale = Vector3.Lerp(startScale, TargetScale, t);
    //transform.localScale = newScale;
 
    // We're done! We can disable this component to save resources.
    //if (t > 1) {
    //  enabled = false;
    //}
    if(transform.localScale.x > 0)
    {
        transform.localScale -= new Vector3(1,1,1) * Time.deltaTime;
    }
    //if (Input.anyKeyDown)
    //{
    //     transform.localScale += new Vector3(1, 1, 1);
    //}

    if(healthScript.CurrentHealth > 200)
    {
      transform.localScale =  new Vector3(10,10,10);
    }

    //if(healthScript.CurrentHealth <= 0)
    //{
    //  transform.localScale = new Vector3(0,0,0);
    //}

    if(transform.localScale.x < 1)
    {
            gameover = true;
            GameoverUI.SetActive(true);
            Time.timeScale = 0;
            
    }

    if(gameover)
    {
      GameoverUI.SetActive(true);
      Time.timeScale = 0;
    }
  }
     //public float maxSize;
     //public float growFactor;
     //public float waitTime;
    // public GameObject fire;
    
    // void Start()
    // {
         
        // transform.localScale = new Vector3(20, 20, 1);
         //StartCoroutine(Scale());
     //}
    
    //void update()
   // {
        //if(healthScript.CurrentHealth > 0)
        //{
          //  fire.transform.localScale -= new Vector3(1, 1, 1) * Time.deltaTime;
            
        //}
   // }
     //IEnumerator Scale()
     //{
        // float timer = 0;
 
        // while(true)
         //{
            // while(healthScript.decrease > 1)
            // {
             //    timer += Time.deltaTime;
            //     transform.localScale += new Vector3(1, 1, 1) * Time.deltaTime * growFactor;
             //    yield return null;
            // }
 
            // yield return new WaitForSeconds(waitTime);
 
             //timer = 0;
            // while(healthScript.decrease < 1)
            // {
            //     timer += Time.deltaTime;
              //   transform.localScale -= new Vector3(1, 1, 1) * Time.deltaTime * growFactor;
                // yield return null;
            // }
 
             //timer = 0;
            // yield return new WaitForSeconds(waitTime);
        // }
     //}
}
