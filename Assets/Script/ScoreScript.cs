using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    
    Text text;
    public Text hs;
    public Text wd;
    public Text sd;
    public static int scorenum;
    public static int winddestroyed;
    public static int slimedestroyed;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        hs.text = "Highscore : " + PlayerPrefs.GetInt("Highscore",0).ToString();
        wd.text = "Highest Wind Destroyed : " + PlayerPrefs.GetInt("Wind Destroyed",0).ToString();
        sd.text = "Highest Slime Destroyed : " + PlayerPrefs.GetInt("Slime Destroyed",0).ToString();
        //winddestroyed = 0;
        //slimedestroyed = 0;
        //hs.text = "Highscore : " + ((int)PlayerPrefs.GetFloat("Highscore")).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Score : " + scorenum.ToString();
        
        
        if(scorenum > PlayerPrefs.GetInt("Highscore",0))
        {
            PlayerPrefs.SetInt("Highscore", scorenum);
            hs.text ="Highscore : " + scorenum.ToString();
        }

        if(winddestroyed > PlayerPrefs.GetInt("Wind Destroyed",0))
        {
            PlayerPrefs.SetInt("Wind Destroyed", winddestroyed);
            wd.text = "Highest Wind Destroyed : " + winddestroyed.ToString();
        }

        if(slimedestroyed > PlayerPrefs.GetInt("Slime Destroyed",0))
        {
            PlayerPrefs.SetInt("Slime Destroyed", slimedestroyed);
            sd.text = "Highest Slime Destroyed : " + slimedestroyed.ToString();
        }
        
        
    }
}
