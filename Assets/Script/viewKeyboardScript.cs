using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class viewKeyboardScript : MonoBehaviour
{
    public GameObject keyboardpanel;
    public GameObject view;
    public GameObject hide;
    public bool isView = false;
    // Start is called before the first frame update
    void Start()
    {
        //isView = false;
    }

    // Update is called once per frame
    void Update()
    {
       if(!isView)
       {
           keyboardpanel.SetActive(false);
           view.SetActive(true);
           hide.SetActive(false);
       }

       if(isView)
       {
           keyboardpanel.SetActive(true);
           view.SetActive(false);
           hide.SetActive(true);
       }
    }

    public void clickview()
    {
        if(!isView)
        {
            isView = true;
        }

        //if(isView)
        //{
        //    isView = false;
        //}
    }
    public void clickhide()
    {
        if(isView)
        {
            isView = false;
        }

        //if(isView)
        //{
        //    isView = false;
        //}
    }
}
